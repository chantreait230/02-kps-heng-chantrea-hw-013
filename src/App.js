import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import axios from "axios";
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import Add from "./components/Add"
import Home from "./components/Home"
import './style/style.css'
import { Button, Nav, Navbar, FormControl, Form } from 'react-bootstrap';
import View from './components/View'
import Update from './components/Update'


export default class App extends Component {
  constructor ( props ) {
    super( props )

    this.state = {
      article: [],
      title: '',
      des: '',
      indexUpdate: 0,
      titleError: false,
      desError: false,
      img: '',
      search: '',
      isSelectImg: false,
      loading:true
    }
  }
  componentWillMount() {
    axios.get( `http://110.74.194.124:15011/v1/api/articles?page=1&limit=15` ).then( ( res ) => {
      this.setState( {
        article: res.data.DATA,
        loading:false
      } )
    } )
  }
  componentWillUpdate(){
    axios.get( `http://110.74.194.124:15011/v1/api/articles?page=1&limit=15` ).then( ( res ) => {
      this.setState( {
        article: res.data.DATA
      } )
    } )
    
  }
  handleChange = ( e ) => {
    this.setState( {
      [ e.target.name ]: e.target.value,
      titleError: false,
      desError: false
    } )
  }

  handleSubmit = ( data ) => {
    if ( this.state.title === "" && this.state.des === "" ) {
      this.setState( {
        titleError: true,
        desError: true
      } )
      return;
    } else if ( this.state.des === "" ) {
      this.setState( {
        desError: true
      } )
      return;
    } else if ( this.state.title === "" ) {
      this.setState( {
        titleError: true
      } )
      return;
    }
    let object = {
      TITLE: data.title,
      DESCRIPTION: data.des,
      IMAGE: data.img,
    }

    axios.post("http://110.74.194.124:15011/v1/api/articles", object ).then( ( res ) => {
      const r = window.confirm( res.data.MESSAGE );
      if ( r == true ) {
        this.setState( {
          title: '',
          des: '',
          img: '',
          isSelectImg: false
        })
      }
    })
    
  }
  handleSearch = ( e ) => {
    this.setState( {
      search: e.target.value.substr( 0, 20 )
    } )

  }
  updateData = ( index, item ) => {

    this.setState( {

      title: item.TITLE,
      des: item.DESCRIPTION,
      img:item.IMAGE

    } )
  }
  handleUpdate = ( id, data ) => {
    if ( this.state.title === "" && this.state.des === "" ) {
      this.setState( {
        titleError: true,
        desError: true
      } )
      return;
    } else if ( this.state.des === "" ) {
      this.setState( {
        desError: true
      } )
      return;
    } else if ( this.state.title === "" ) {
      this.setState( {
        titleError: true
      } )
      return;
    }
    let object = {
      TITLE: data.title,
      DESCRIPTION: data.des,
      IMAGE: data.img
    }
    axios.put( `http://110.74.194.124:15011/v1/api/articles/${id}`, object ).then( ( res ) => {

      const r = window.confirm( res.data.MESSAGE );

      if ( r == true ) {
        this.setState( {
          title: '',
          des: '',
          img: '',
          isSelectImg: false
        })
      }

    } )
  }
  handleDeleteRow = ( id ) => {

    axios.delete( `http://110.74.194.124:15011/v1/api/articles/${id}` ).then( ( res ) => {

      this.setState( {
        article: [ ...this.state.article.filter( ( a ) => a.ID !== id ) ]
      } )
      alert( res.data.MESSAGE )

    } )

  }
  // onImageChange = ( event ) => {
  //   if ( event.target.files && event.target.files[ 0 ] ) {
  //     let reader = new FileReader();
  //     reader.onload = ( e ) => {
  //       this.setState( { img: e.target.result, isSelectImg: true } );
  //     };
  //     reader.readAsDataURL( event.target.files[ 0 ] );
  //   }
  // }

  render() {
    let filterArticle = this.state.article.filter(
      ( article ) => {
        return article.TITLE.toLowerCase().indexOf( this.state.search.toLowerCase() ) !== -1
      }
    );
    return (
      <div>
        <Router>
          <Navbar bg="light" expand="lg">
            <Navbar.Brand as={ Link } to="/">Article</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="mr-auto">
                <Nav.Link as={ Link } to="/home">Home</Nav.Link>
              </Nav>
              <Form inline>
                <FormControl
                  type="text"
                  placeholder="Search"
                  className="mr-sm-2"
                  value={ this.state.search }
                  onChange={ e => this.handleSearch( e ) } />
                <Button variant="outline-success">Search</Button>
              </Form>
            </Navbar.Collapse>
          </Navbar>

          <Switch>
            <Route path="/" exact 
                render={ () => 
                <Home fromApp={ filterArticle } 
                onUpdateHome={ this.updateData } 
                onDeleteHome={ this.handleDeleteRow }
                load={this.state.loading}
                 /> } />


            <Route path="/home" render={ () => 
              <Home fromApp={ filterArticle } 
              onUpdateHome={ this.updateData } 
              onDeleteHome={ this.handleDeleteRow } 
              loading={this.state.loading}
              /> } />


            <Route path="/view/:ID" render={ ( props ) => <View { ...props } toView={ this.state.article } /> } />


            <Route path="/add" render={ () =>
              <Add
                onChanged={ this.handleChange }
                // filterImage={ this.onImageChange }
                dataParent={ this.state }
                onSubmit={ this.handleSubmit }
              /> }
            />
            <Route path="/update/:ID" render={ ( props ) =>
              <Update
                { ...props }
                onChanged={ this.handleChange }
                dataParent={ this.state }
                // filterImage={ this.onImageChange }
                toUpdate={ this.state.article }
                onUpdateData={ this.handleUpdate }
              /> }
            />
          </Switch>
        </Router>
      </div>
    )
  }
}

