import React from 'react'
import { Button} from 'react-bootstrap';
import { Link } from "react-router-dom";
import ArticleList from './ArticleList'
export default function Home(props) {
    return (
        <div>
            <div className="container">
                <h1 style={{textAlign:'center',fontFamily: "'Courgette', cursive"}}>Article Management</h1>
                <div className="row">
                    <div className="col-lg-3">
                        <Link to="/add"><Button variant="primary" style={{ marginBottom:"10px" }}>Add</Button></Link>
                    </div>
                    {/* <div className="col-lg-9">
                        <ul>
                            <li onClick={()=>props.onBack()}>Back</li>
                            <li>1/10</li>
                            <li onClick={()=>props.onNext()}>Next</li>
                        </ul>
                    </div> */}
                </div>
                
                <ArticleList fromHome={props.fromApp} loader={props.load} onUpdate={props.onUpdateHome} onDelete={props.onDeleteHome}/>
            </div>
            
        </div>
    )
}
