import React from 'react'
import { Table,Button} from 'react-bootstrap';
import { Link } from "react-router-dom";
import '../style/style.css'

export default function ArticleList(props) {
   
    function convertToDate(date){
        let paramDate=date;
        let year=paramDate.substring(0,4);
        let month=paramDate.substring(4,6);
        let day=paramDate.substring(6,8);
        let fullDate=year+"-"+month+"-"+day;
        return fullDate;
    }
    if(props.loader===true){
        return <div className="loader"></div>;
    }
    console.log(props.loader)
    const row=props.fromHome.map((data,index)=>
        <tr key={data.ID}>
            <td>{data.ID}</td>
            <td style={{ fontFamily: "'Hanuman', serif" }}>{data.TITLE}</td>
            <td style={{ fontFamily: "'Hanuman', serif" }}>{data.DESCRIPTION}</td>
            <td style={{ fontFamily: "'Hanuman', serif" }}>{convertToDate(data.CREATED_DATE)}</td>
            <td>
                <div className="Box">
                    {data.IMAGE===null ?
                        <img src="https://lunawood.com/wp-content/uploads/2018/02/placeholder-image.png" alt="No_Photo"/>
                        :
                        <img src={data.IMAGE} alt="No_Photo"/>
                    }
                    
                </div>
            </td>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
            <td style={{width:'260px'}}> 
                <Link to={`/view/${data.ID}`}><Button variant="primary">View</Button></Link>
                <Link to={`/update/${data.ID}`}><Button variant="warning" className="mx-1" onClick={()=>props.onUpdate(index,data)}>Edit</Button></Link>
                <Button
                    variant="danger"
                    onClick={()=>props.onDelete(data.ID)}
                    >Delete
                </Button>
            </td>
        </tr>
    )
    return (
        <div>
            <Table striped bordered hover>
                <thead style={{ fontFamily: "'Laila', serif" }}>
                    <tr>
                    <th>#</th>
                    <th>TITLE</th>
                    <th>DECRIPTION</th>
                    <th>CREATE_DATE</th>
                    <th>IMAGE</th>
                    <th>ACTION</th>
                    </tr>
                </thead>
                <tbody>
                   {row}
                </tbody>
            </Table>
        </div>
    )
}
