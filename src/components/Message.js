import React from 'react'
import { Button, Modal,NavDropdown } from 'react-bootstrap';

export default function Message(props) {
    const [show, setShow] = React.useState(false);
  
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    return (
      <>
        <NavDropdown.Item onClick={handleShow}>Delete</NavDropdown.Item>
        <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton>
        <Modal.Title>Do you want to Delete ?</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              OK
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    );
}