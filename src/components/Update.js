import React from 'react'
import { Button} from 'react-bootstrap';
// import { Link } from "react-router-dom";
import '../style/style.css'

export default function Update(props) {
    var dataUpdate=props.toUpdate.find((item)=> item.ID==props.match.params.ID);
    return (
        <div className="container">
            <div className="frm">
                <div className="headerfrm">
                    <span>Article Info</span>
                </div>
                <form className="upl">
                    <div className="body">
                        <div className="row">
                            <div className="col-md-8">
                                <div className="form-group">
                                    <label for="exampleInputEmail1">TITLE: <span id="span-name" className="text-danger"></span></label>
                                    <input 
                                        type="text" 
                                        className="form-control"                                                                                                   
                                        placeholder="Title"
                                        name="title"
                                        value={props.dataParent.title}
                                        onChange={e => props.onChanged(e)}
                                    />
                                    <label className="sms">{props.dataParent.titleError?"!Validate title":null}</label>
                                </div>
                                <div className="form-group">
                                    <label for="exampleInputPassword1">DESCRIPTION: <span id="span-phone" className="text-danger"></span></label>
                                     <textarea
                                        style={{height:"160px"}}
                                        className="form-control" 
                                        placeholder="Description"
                                        name="des"
                                        value={props.dataParent.des}
                                        onChange={e => props.onChanged(e)}
                                     ></textarea>
                                    <label className="sms">{props.dataParent.desError?"!Validate description":null}</label>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="imgBox">
                                    
                                    {props.dataParent.isSelectImg ? 
                                        <img  id="target" src={props.dataParent.img} alt="pic" /> 
                                        : 
                                        <img id="target" src={dataUpdate.IMAGE} alt="pic"/>
                                    }
                                </div>
                                <label for="group_image" className="btnBrowse">Browse</label>
                                {/* <input type="file" onChange={(e)=>props.filterImage(e)} className="filetype" id="group_image"/> */}
                            </div>
                        </div>
                    </div>
                    <div className="footer">
                        <Button 
                            variant="primary" 
                            className="btnAdd"
                            onClick={()=> props.onUpdateData(dataUpdate.ID,props.dataParent)}
                        >
                            Submit
                        </Button>
                        
                    </div>
                </form>
            </div>
        </div>
    )
}
