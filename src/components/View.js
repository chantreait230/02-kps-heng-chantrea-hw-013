import React from 'react'
import '../style/style.css'

export default function View(props) {
    var dataView=props.toView.find((item)=> item.ID==props.match.params.ID);
    return (
        <div className="container">
            <div>
                <form className="upl">
                    <div className="body">
                        <div className="row">
                            <div className="col-md-8">
                                <div className="form-group">
                                    <label for="exampleInputEmail1">TITLE: <span id="span-name" className="text-danger"></span></label>
                                    <input 
                                        type="text" 
                                        className="form-control"
                                        value={dataView.TITLE}
                                    />
                                </div>
                                <div className="form-group">
                                    <label for="exampleInputPassword1">DESCRIPTION: <span id="span-phone" className="text-danger"></span></label>
                                     <textarea
                                        style={{height:"160px"}}
                                        className="form-control"
                                        value={dataView.DESCRIPTION}
                                     ></textarea>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="imgBox" style={{marginTop:"32px",height:"245px",width:"300px"}}>
                                    
                                    {dataView.IMAGE===null ? 
                                        
                                        <img id="target" src="https://lunawood.com/wp-content/uploads/2018/02/placeholder-image.png" alt="pic"/>
                                        : 
                                        <img  id="target" src={dataView.IMAGE} alt="pic-not-font" /> 
                                    }

                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    )
}
